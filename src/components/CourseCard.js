import { Button, Card } from 'react-bootstrap';
import { useState, useEffect } from 'react';

export default function CourseCard(prop) {
    const [count, setCount] = useState(0);
    const [seats, setSeats] = useState(30);
    const [isDisabled, setDisabled] = useState(false);

    useEffect(() => {
        if (seats == 0) setDisabled(true);
    }, [seats]);

    function seatOccupation() {
        setCount(count + 1);
        setSeats(seats - 1);
    }

    return (
        <Card className="my-5 cardHighlight p-3">
            <Card.Body>
                <Card.Title>
                    <h2>{prop.name}</h2>
                </Card.Title>
                <Card.Text>
                    <strong>Description:</strong>
                    <p>{prop.description}</p>
                    <strong>Price:</strong>
                    <p>PHP {prop.price}</p>
                    <p><strong>Enrollees: {count}</strong></p>
                    <Button variant="primary" disabled={isDisabled} onClick={seatOccupation}>Enroll Now!</Button>
                </Card.Text>
            </Card.Body>
        </Card>
    )
}
