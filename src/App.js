import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home";
import { Container } from "react-bootstrap";
import Courses from "./pages/Courses";
import Register from "./pages/Register";
import Login from "./pages/Login";

function App() {
  return (
    <div className="App">
      <AppNavbar />
      <Container>
        {/* <Home />
        <Courses /> */}
        {/* <Register /> */}
        <Login />
      </Container>
    </div>
  );
}

export default App;
